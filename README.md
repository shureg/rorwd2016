# Ruby on Rails Web Development Specialization

by Johns Hopkins University

+ Ruby on Rails: An Introduction
+ Rails with Active Record and Action Pack
+ Ruby on Rails Web Services and Integration with MongoDB
+ HTML, CSS, and Javascript for Web Developers
+ Single Page Web Applications with AngularJS

+ Capstone: Photo Tourist Web Application



## Resources:

here you will find some links to the interesting ressourses mentioned in that courses

### Books

| **Title**                                | **Author**               | **Description**                          |
| ---------------------------------------- | ------------------------ | ---------------------------------------- |
| [**Mastering Sublime Text**](http://amzn.to/2b0j8sB) | Dan Peleg                | Einfache Schrittfür Schritt Anleitung für Sublime Editor |
| [**Pro Git**](http://amzn.to/2ay9fFR)    | Scott Chacon, Ben Straub | Gutes Buch über Git. Online Version for [free](https://git-scm.com/book/en/v2) |
| [**Git in Practice**](http://amzn.to/2b0jUWr) | Mike McQuaid             | Gutes Nachschlagwerg, Alles was man zum Thema Git wissen muss. |
| [**Learn Git in a Month of Lunches**](http://amzn.to/2b0jX4z) | Rick Umali               | Git für Fortgeschrittene                 |
| [**Pragmatic Guide to Git**](http://amzn.to/2b0ki7i) | Travis Swicegood         | Weiteres gutes Buch über Git             |