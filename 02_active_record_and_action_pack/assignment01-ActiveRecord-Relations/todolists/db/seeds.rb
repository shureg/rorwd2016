# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Profile.delete_all
TodoList.delete_all
TodoItem.delete_all
User.delete_all

# Create User
User.create! [
    {username:"Fiorina", password_digest:"123"},
    {username:"Trump", password_digest:"234"},
    {username:"Carson", password_digest:"345"},
    {username:"Clinton", password_digest:"56"}
             ]
# Create Profile
User.find_by!(username: 'Fiorina').create_profile(gender: 'female', birth_year: 1954, first_name: 'Carly', last_name: 'Fiorina')
User.find_by!(username: 'Trump').create_profile(gender: 'male', birth_year: 1946, first_name: 'Donald', last_name: 'Trump')
User.find_by!(username: 'Carson').create_profile(gender: 'male', birth_year: 1951, first_name: 'Ben', last_name: 'Carson')
User.find_by!(username: 'Clinton').create_profile(gender: 'female', birth_year: 1947, first_name: 'Hillary', last_name: 'Clinton')



User.first.todo_lists.create! [
                                  { list_name: 'List1', list_due_date: Date.today + 3.week }
                              ]
User.second.todo_lists.create! [
                                   { list_name: 'List2', list_due_date: Date.today + 1.week }
                               ]
User.third.todo_lists.create! [
                                  { list_name: 'List3', list_due_date: Date.today + 2.month }
                              ]
User.fourth.todo_lists.create! [
                                   { list_name: 'List4', list_due_date: Date.today + 1.year }
                               ]


# Create Todo Items


TodoList.first.todo_items.create! [
                                      { title: '1', description: 'first description.', due_date: Date.today + 1.year },
                                      { title: '2', description: 'second description.', due_date: Date.today + 1.year },
                                      { title: '3', description: 'third description.', due_date: Date.today + 1.year },
                                      { title: '4', description: 'fourth description.', due_date: Date.today + 1.year },
                                      { title: '5', description: 'fifth description.', due_date: Date.today + 1.year },
                                  ]
TodoList.second.todo_items.create! [
                                       { title: '1', description: 'first description.', due_date: Date.today + 1.year },
                                       { title: '2', description: 'second description.', due_date: Date.today + 1.year },
                                       { title: '3', description: 'third description.', due_date: Date.today + 1.year },
                                       { title: '4', description: 'fourth description.', due_date: Date.today + 1.year },
                                       { title: '5', description: 'fifth description.', due_date: Date.today + 1.year },
                                   ]
TodoList.third.todo_items.create! [
                                      { title: '1', description: 'first description.', due_date: Date.today + 1.year },
                                      { title: '2', description: 'second description.', due_date: Date.today + 1.year },
                                      { title: '3', description: 'third description.', due_date: Date.today + 1.year },
                                      { title: '4', description: 'fourth description.', due_date: Date.today + 1.year },
                                      { title: '5', description: 'fifth description.', due_date: Date.today + 1.year },
                                  ]
TodoList.fourth.todo_items.create! [
                                       { title: '1', description: 'first description.', due_date: Date.today + 1.year },
                                       { title: '2', description: 'second description.', due_date: Date.today + 1.year },
                                       { title: '3', description: 'third description.', due_date: Date.today + 1.year },
                                       { title: '4', description: 'fourth description.', due_date: Date.today + 1.year },
                                       { title: '5', description: 'fifth description.', due_date: Date.today + 1.year },
                                   ]
