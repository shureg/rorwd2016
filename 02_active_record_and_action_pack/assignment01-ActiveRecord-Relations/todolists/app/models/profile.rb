class Profile < ActiveRecord::Base
  belongs_to :user

  validates :gender, inclusion: ['female', 'male']
  validate :names_not_null
  validate :not_sue_as_first_name

  def names_not_null
    if last_name.nil? && first_name.nil?
      errors.add(:last_name, 'last name and first name can''t both be null!')
    end
  end

  def not_sue_as_first_name
    if gender == 'male' and first_name == 'Sue'
      errors.add(:first_name, 'male can''t have Sue as a first name !')
    end
  end

  def self.get_all_profiles min, max
    Profile.where('birth_year between ? and ?', min, max).order(birth_year: :asc)

  end
end
