class Person
  attr_accessor :first_name , :last_name 
  @@people = []
  def initialize(first_name, last_name)
    @first_name = first_name
	@last_name = last_name
	@@people << self
  end
  
  def self.search (last_name)
    @@people.select {|person| person.last_name === last_name}
  end
  
  def to_s 
    "#{@first_name} #{@last_name}"
  end
  


end

p1 = Person.new("A","B")
p2 = Person.new("C","B")
p3 = Person.new("A","Q")

p p1

p Person.search("B")