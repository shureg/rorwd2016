require 'httparty'

class Recipe
  include HTTParty
  
  base_uri "http://food2fork.com/api"
  default_params key: ENV["FOOD2FORK_KEY"]
  format :json

  def self.for item
    get("/search", query:{q:item})["recipes"]
  end
end
