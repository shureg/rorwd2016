class CoursesController < ApplicationController
  def index
    @course = params[:search]||"java"
    @courseList = Coursera.for (@course)
  end
end
