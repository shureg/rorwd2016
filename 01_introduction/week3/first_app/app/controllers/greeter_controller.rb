class GreeterController < ApplicationController
  def hello
    arr_names = ["Alex","Peter","Marc"]
    @name = arr_names.sample
    @time = Time.now()
    @time_displayed ||= 0
    @time_displayed += 1
  end
end
