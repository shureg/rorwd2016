class RecipesController < ApplicationController
  def index
    @recipe = params[:search]||"chocolate"
    @recipeList = Recipe.for (@recipe)
  end
end
