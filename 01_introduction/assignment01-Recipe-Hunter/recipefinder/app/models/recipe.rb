class Recipe
  include HTTParty

  keyvalue = ENV['FOOD2FORK_KEY']
  hostport = ENV['FOOD2FORK_SERVER_AND_PORT'] || 'www.food2fork.com'
  base_uri "http://#{hostport}/api"
  default_params key: keyvalue
  format :json

  def self.for item
    get("/search", query:{q:item})["recipes"]
  end
end
